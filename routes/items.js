const express = require('express');
const router = express.Router({ mergeParams: true });
const rp = require('request-promise');

/* GET item page. */
router.get('/', function(req, res, next) {
    const query = req.params.query;
    const allItems = {
        uri: 'https://api.mercadolibre.com/sites/MLA/search?q=' + query,
        headers: {
            'User-Agent': 'Request-Promise'
        },
        json: true // Automatically parses the JSON string in the response
    };
     
    rp(allItems)
        .then(function (items) {
                var items = items.results.slice(0,4);
                var itemsDetails = items.map((item) => {
                const amount = Math.floor(item.price);
                const decimals = +(item.price%1).toFixed(2).substring(2);
                return {
                    id: item.id,
                    title: item.title,
                    price: {
                        currency: item.currency_id,
                        amount: amount,
                        decimals: decimals
                    },
                    picture: item.thumbnail,
                    condition: item.condition,
                    free_shipping: item.shipping ? item.shipping.free_shipping : false,
                    address: item.address ? item.address.state_name : ''
                }
            });
            res.json(itemsDetails);
        })
        .catch(function (err) {
            console.log('Error', err);
        });
        
});

module.exports = router;