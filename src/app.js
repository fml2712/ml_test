import React from 'react';
import ReactDOM from 'react-dom';

const title = 'Mi simple ambiente express react webpack babel';

ReactDOM.render(
  <div>{title}</div>,
  document.getElementById('app')  
);
