const express = require('express');
const path = require('path');
var homeRouter = require('./routes/index');
var itemsRouter = require('./routes/items');
var itemRouter = require('./routes/item');

const publicDir = express.static(`${__dirname}/public/`);
const port = (process.env.PORT || 3005);
const viewDir = `${__dirname}/views/`;
const app = express();

app.set('view', viewDir);
app.set('view engine', 'pug');
app.set('port', port);

app.use(publicDir);
app.use('/', homeRouter);
app.use('/items/:query', itemsRouter);
app.use('/item/:id', itemRouter);

app.listen(app.get('port'), () =>{
    console.log(`Listening port ${app.get('port')}`);
});